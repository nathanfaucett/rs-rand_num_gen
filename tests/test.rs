extern crate rand_num_gen;


use rand_num_gen::{Rng, XorShift};


#[test]
fn test_xorshift_random_byte_iter() {
    let mut rng = XorShift::default();
    let bytes: Vec<u8> = rng.random_byte_iter().take(32).collect();

    assert_eq!(&*bytes, &[
        15, 98, 241, 219, 132, 225, 99, 77,
        11, 61, 31, 194, 140, 92, 213, 14,
        67, 166, 100, 94, 202, 3, 89, 12,
        47, 144, 149, 55, 109, 155, 0, 147
    ]);
}
