use core::num::Wrapping;

use super::{Rng, SeedableRng};


// http://en.wikipedia.org/wiki/Xorshift
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct XorShift {
    x: Wrapping<u32>,
    y: Wrapping<u32>,
    z: Wrapping<u32>,
    w: Wrapping<u32>,
}

impl Default for XorShift {
    #[inline(always)]
    fn default() -> Self {
        SeedableRng::from_seed([0x193a6754, 0xa8a7d469, 0x97830e05, 0x113ba7bb])
    }
}

impl XorShift {

    #[cfg(target_pointer_width="32")]
    #[inline(always)]
    pub fn new() -> XorShift {
        let a = &false as *const _ as u32;
        let b = a.wrapping_mul(a);

        let x = a;
        let y = b;
        let z = (a << 16) | (b >> 16);
        let w = (b << 16) | (a >> 16);

        SeedableRng::from_seed([x, y, z, w])
    }

    #[cfg(target_pointer_width="64")]
    #[inline(always)]
    pub fn new() -> XorShift {
        let p = &false as *const _ as u64;

        let a = (p >> 32) as u32;
        let b = p as u32;
        let c = a.wrapping_mul(b);

        let x = (a << 16) | (b >> 16);
        let y = (b << 16) | (c >> 16);
        let z = (c << 16) | (a >> 16);
        let w = (b << 16) | (a >> 16);

        SeedableRng::from_seed([x, y, z, w])
    }
}

impl SeedableRng<[u32; 4]> for XorShift {
    #[inline(always)]
    fn from_seed(seed: [u32; 4]) -> XorShift {
        XorShift {
            x: Wrapping(seed[0]),
            y: Wrapping(seed[1]),
            z: Wrapping(seed[2]),
            w: Wrapping(seed[3]),
        }
    }
    #[inline(always)]
    fn seed(&self) -> [u32; 4] {
        [self.x.0, self.y.0, self.z.0, self.y.0]
    }
    #[inline]
    fn set_seed(&mut self, seed: [u32; 4]) {
        self.x = Wrapping(seed[0]);
        self.y = Wrapping(seed[1]);
        self.z = Wrapping(seed[2]);
        self.w = Wrapping(seed[3]);
    }
}

impl Rng for XorShift {
    #[inline]
    fn next_u32(&mut self) -> u32 {
        let t = self.x ^ (self.x << 11);
        self.x = self.y;
        self.y = self.z;
        self.z = self.w;
        let w = self.w;
        self.w = w ^ (w >> 19) ^ (t ^ (t >> 8));
        self.w.0
    }
}


#[test]
fn test_xorshift() {
    let mut xorshift = XorShift::default();
    assert_eq!(xorshift.next_u32(), 3690029583);
}
