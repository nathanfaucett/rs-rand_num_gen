#![feature(test)]


extern crate test;

extern crate rand;
extern crate rand_num_gen;


use test::Bencher;


static SIZE: usize = 1024 * 8;


#[bench]
fn test_rand_xorshift(b: &mut Bencher) {
    use rand::Rng;

    let mut rng = rand::weak_rng();

    b.iter(move || {
        for _ in 0..SIZE {
            test::black_box(rng.next_u32());
        }
    });
}

#[bench]
fn test_rand_num_gen_xorshift(b: &mut Bencher) {
    use rand_num_gen::Rng;

    let mut rng = rand_num_gen::XorShift::new();

    b.iter(move || {
        for _ in 0..SIZE {
            test::black_box(rng.next_u32());
        }
    });
}


#[bench]
fn test_rand_fill_bytes(b: &mut Bencher) {
    use rand::Rng;

    let mut rng = rand::weak_rng();
    let mut bytes = [0u8; 1024 * 8];

    b.iter(move || {
        rng.fill_bytes(&mut bytes);
    });
}

#[bench]
fn test_rand_num_gen_fill_bytes(b: &mut Bencher) {
    use rand_num_gen::Rng;

    let mut rng = rand_num_gen::XorShift::new();
    let mut bytes = [0u8; 1024 * 8];

    b.iter(move || {
        rng.fill_bytes(&mut bytes);
    });
}
