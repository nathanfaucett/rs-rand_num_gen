extern crate rand_num_gen;
extern crate imagefmt;


use rand_num_gen::{Rng, XorShift};
use imagefmt::{ColFmt, ColType};


const SIZE: usize = 1024;
const ITEM_SIZE: usize = 3;


fn rand_num_gen_put<R: Rng>(rng: &mut R, bytes: &mut [u8]) {
    let mut iter = rng.random_byte_iter();

    for byte in bytes {
        *byte = iter.next().unwrap();
    }
}


fn main() {
    let mut bytes = [0u8; SIZE * SIZE * ITEM_SIZE];

    rand_num_gen_put(&mut XorShift::new(), &mut bytes);
    let _ = imagefmt::write("examples/xorshift.png", SIZE, SIZE, ColFmt::RGB, &bytes, ColType::Color);
}
