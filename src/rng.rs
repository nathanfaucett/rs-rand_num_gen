use core::mem;

use super::{Random, RandomIter, RandomByteIter};


pub trait Rng {

    fn next_u32(&mut self) -> u32;

    #[inline(always)]
    fn next_u64(&mut self) -> u64 {
        ((self.next_u32() as u64) << 32) | (self.next_u32() as u64)
    }

    #[cfg(target_pointer_width = "32")]
    #[inline(always)]
    fn next_usize(&mut self) -> usize { self.next_u32() as usize }

    #[cfg(target_pointer_width = "64")]
    #[inline(always)]
    fn next_usize(&mut self) -> usize { self.next_u64() as usize }

    #[inline]
    fn next_f32(&mut self) -> f32 {
        const UPPER_MASK: u32 = 0x3F800000;
        const LOWER_MASK: u32 = 0x7FFFFF;

        let tmp = UPPER_MASK | (self.next_u32() & LOWER_MASK);
        let result: f32 = unsafe { mem::transmute(tmp) };
        result - 1.0
    }
    #[inline]
    fn next_f64(&mut self) -> f64 {
        const UPPER_MASK: u64 = 0x3FF0000000000000;
        const LOWER_MASK: u64 = 0xFFFFFFFFFFFFF;

        let tmp = UPPER_MASK | (self.next_u64() & LOWER_MASK);
        let result: f64 = unsafe { mem::transmute(tmp) };
        result - 1.0
    }

    #[inline(always)]
    fn next<T>(&mut self) -> T
        where T: Random,
              Self: Sized,
    {
        Random::random(self)
    }

    #[inline(always)]
    fn random_iter<'a, T>(&'a mut self) -> RandomIter<'a, T, Self>
        where T: Random,
              Self: Sized,
    {
        RandomIter::new(self)
    }

    #[inline(always)]
    fn random_byte_iter<'a>(&'a mut self) -> RandomByteIter<'a, Self>
        where Self: Sized,
    {
        RandomByteIter::new(self)
    }

    #[inline]
    fn fill_bytes(&mut self, bytes: &mut [u8]) {
        let mut num = self.next_u32();
        let mut count = 4;

        for byte in bytes {
            if count == 0 {
                num = self.next_u32();
                count = 4;
            }

            *byte = (num & 0xff) as u8;

            num >>= 8;
            count -= 1;
        }
    }
}
