rs-rand_num_gen
=====

random number generator

```rust
extern crate rand_num_gen;


use rand_num_gen::{Rng, XorShift};


fn main() {
    let mut rng = XorShift::new();
    let x: f32 = rng.next();
    let y = rng.next_u32();
    let z = rng.next_usize();
    let w = rng.next_f64();
    println!("{:?} {:?} {:?} {:?}", x, y, z, w);
}
```
