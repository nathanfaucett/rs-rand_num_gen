use super::Rng;


pub trait SeedableRng<T>: Rng {
    fn from_seed(T) -> Self;
    fn seed(&self) -> T;
    fn set_seed(&mut self, T);
}
