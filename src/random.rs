use core::num::Wrapping;

use super::Rng;


pub trait Random {
    fn random<R: Rng>(rng: &mut R) -> Self;
}


impl Random for u8 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() as u8 }
}
impl Random for u16 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() as u16 }
}
impl Random for u32 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() }
}
impl Random for u64 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u64() }
}
impl Random for usize {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_usize() }
}


impl Random for i8 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() as i8 }
}
impl Random for i16 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() as i16 }
}
impl Random for i32 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u32() as i32 }
}
impl Random for i64 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_u64() as i64 }
}
impl Random for isize {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_usize() as isize }
}


impl Random for f32 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_f32() }
}
impl Random for f64 {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Self { rng.next_f64() }
}


impl Random for char {
    #[inline]
    fn random<R: Rng>(rng: &mut R) -> Self {
        use core::char;

        const CHAR_MASK: u32 = 0x001f_ffff;

        loop {
            match char::from_u32(rng.next_u32() & CHAR_MASK) {
                Some(c) => return c,
                None => (),
            }
        }
    }
}

impl Random for bool {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> bool { rng.next_u32() & 1 == 1 }
}

impl<T: Random> Random for Option<T> {
    #[inline]
    fn random<R: Rng>(rng: &mut R) -> Option<T> {
        if rng.next() {
            Some(rng.next())
        } else {
            None
        }
    }
}

impl<T: Random> Random for Wrapping<T> {
    #[inline(always)]
    fn random<R: Rng>(rng: &mut R) -> Wrapping<T> { Wrapping(rng.next()) }
}

impl Random for () {
    #[inline(always)]
    fn random<R: Rng>(_: &mut R) -> () { () }
}

macro_rules! impl_tuple {
    ($T:ident) => (
        impl<$T: Random> Random for ($T,) {
            #[inline]
            fn random<RNG: Rng>(rng: &mut RNG) -> ($T,) {
                (rng.next::<$T>(),)
            }
        }
    );
    ($A:ident, $($T:ident),*) => (
        impl<$A: Random, $($T: Random),*> Random for ($A, $($T),* , ) {
            #[inline]
            fn random<RNG: Rng>(rng: &mut RNG) -> ($A, $($T),* , ) {
                (rng.next::<$A>(), $(rng.next::<$T>()),* , )
            }
        }
        impl_tuple!($($T),*);
    );
}

impl_tuple!(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z);


macro_rules! impl_array {
    ($N:expr,) => (
        impl<T> Random for [T; $N] {
            #[inline(always)]
            fn random<R: Rng>(_: &mut R) -> [T; $N] { [] }
        }
    );
    ($N:expr, $T:ident, $($Ts:ident,)*) => (
        impl_array!(($N - 1), $($Ts,)*);

        impl<T> Random for [T; $N] where T: Random {
            #[inline]
            fn random<R: Rng>(rng: &mut R) -> [T; $N] {
                [rng.next::<$T>(), $(rng.next::<$Ts>()),*]
            }
        }
    );
}

impl_array!(32, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,);


#[test]
fn test_tuple() {
    use super::{Rng, XorShift};

    let mut rng = XorShift::default();
    let (x, y, z, w) = rng.next::<(f32, bool, Option<isize>, u8)>();
    assert_eq!(x, 0.885805);
    assert_eq!(y, false);
    assert_eq!(z, Some(1068862244519192131));
    assert_eq!(w, 202);
}

#[test]
fn test_array() {
    use super::{Rng, XorShift};

    let mut rng = XorShift::default();
    let bytes: [u32; 4] = rng.next();
    assert_eq!(bytes, [3690029583, 1298391428, 3256827147, 248863884]);
}
