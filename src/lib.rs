#![no_std]


#[cfg(feature = "use_xorshift")] mod xorshift;

mod random_byte_iter;
mod random_iter;
mod random;
mod seedable_rng;
mod rng;


#[cfg(feature = "use_xorshift")] pub use self::xorshift::XorShift;

pub use self::random_byte_iter::RandomByteIter;
pub use self::random_iter::RandomIter;
pub use self::random::Random;
pub use self::seedable_rng::SeedableRng;
pub use self::rng::Rng;
