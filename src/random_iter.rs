use core::marker::PhantomData;

use super::{Rng, Random};


pub struct RandomIter<'a, T, R>
    where T: 'a + Random,
          R: 'a + Rng,
{
    rng: &'a mut R,
    _marker: PhantomData<&'a (T, R)>,
}

impl<'a, T, R> RandomIter<'a, T, R>
    where T: 'a + Random,
          R: 'a + Rng,
{
    #[inline(always)]
    pub fn new(rng: &'a mut R) -> Self {
        RandomIter {
            rng: rng,
            _marker: PhantomData,
        }
    }
}

impl<'a, T, R> Iterator for RandomIter<'a, T, R>
    where T: 'a + Random,
          R: 'a + Rng,
{
    type Item = T;

    #[inline(always)]
    fn next(&mut self) -> Option<T> {
        Some(self.rng.next())
    }
}
