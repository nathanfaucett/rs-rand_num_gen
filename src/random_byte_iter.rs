use super::Rng;


pub struct RandomByteIter<'a, R>
    where R: 'a + Rng,
{
    count: u8,
    num: u32,
    rng: &'a mut R,
}

impl<'a, R> RandomByteIter<'a, R>
    where R: 'a + Rng,
{
    #[inline(always)]
    pub fn new(rng: &'a mut R) -> Self {
        RandomByteIter {
            count: 0,
            num: 0,
            rng: rng,
        }
    }
}

impl<'a, R> Iterator for RandomByteIter<'a, R>
    where R: 'a + Rng,
{
    type Item = u8;

    #[inline(always)]
    fn next(&mut self) -> Option<u8> {
        if self.count == 0 {
            self.num = self.rng.next_u32();
            self.count = 4;
        }

        let byte = (self.num & 0xff) as u8;

        self.num >>= 8;
        self.count -= 1;

        Some(byte)
    }
}
